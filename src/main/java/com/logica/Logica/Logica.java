package com.logica.Logica;

public class Logica {

	public static long nextLookAndSeeNumber(long value) {
		
		char[] listValue = String.valueOf(value).toCharArray();
		int count = 0;
		int comparableValue = Character.getNumericValue(listValue[0]);
		String nextValue = "";
		
		for(int i = 0; i < listValue.length; i++) {
			
			if(Character.getNumericValue(listValue[i]) < 1 || Character.getNumericValue(listValue[i]) > 3) {
				return -1;
			}
			
			if(Character.getNumericValue(listValue[i]) == comparableValue) {
				count++;
			}
			
			else {
				nextValue = nextValue + count + comparableValue;
				comparableValue = Character.getNumericValue(listValue[i]);
				count = 1;
			}
			
			if(i == listValue.length - 1) {
				nextValue = nextValue + count + comparableValue;
			}
		}
		
		return Long.valueOf(nextValue);
	}
	
	public static boolean isDateValid(int day, int month, int year) {
		if( (day >= 1 && day <= 31 && month >= 1 && month <= 12 && year >= 1) &&
			(day <= 31 && (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)) ||
			(day <= 30 && (month != 2)) ||
			(day <= 29 && month == 2 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) ) ||
			(day <= 28)) 
		{	
			return true;
		}
		
		else {
			return false;
		}
	}
	
	public static String toRomanNumeral(int number) {
		
		String romanNumeral = "";
		
		while(number >= 1000) {
			number -= 1000;
			romanNumeral += "M";
		}
		
		while(number >= 900) {
			number -= 900;
			romanNumeral += "CM";
		}
		
		while(number >= 500) {
			number -= 500;
			romanNumeral += "D";
		}
		
		while(number >= 400) {
			number -= 400;
			romanNumeral += "CD";
		}
		
		while(number >= 100) {
			number -= 100;
			romanNumeral += "C";
		}
		
		while(number >= 90) {
			number -= 90;
			romanNumeral += "XC";
		}
		
		while(number >= 50) {
			number -= 50;
			romanNumeral += "L";
		}
		
		while(number >= 40) {
			number -= 40;
			romanNumeral += "XL";
		}
		
		while(number >= 10) {
			number -= 10;
			romanNumeral += "X";
		}
		
		while(number >= 9) {
			number -= 9;
			romanNumeral += "IX";
		}
		
		while(number >= 5) {
			number -= 5;
			romanNumeral += "V";
		}
		
		while(number >= 4) {
			number -= 4;
			romanNumeral += "IV";
		}
		
		while(number >= 1) {
			number -= 1;
			romanNumeral += "I";
		}
		
		return romanNumeral;
	}
}
