package com.logica.Logica;

import com.logica.Logica.Logica;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LogicaTests {
	
	@Test
	public void testNextLookAndSeeNumber1() {
		
		int firstValue = 1;
		int nextValue = 11;
		
		assertTrue(Logica.nextLookAndSeeNumber(firstValue) == nextValue);
	}
	
	@Test
	public void testNextLookAndSeeNumber2() {
		
		long firstValue = 11;
		long nextValue = 21;
		
		assertTrue(Logica.nextLookAndSeeNumber(firstValue) == nextValue);
	}
	
	@Test
	public void testNextLookAndSeeNumber3() {
		
		long firstValue = 21;
		long nextValue = 1211;
		
		assertTrue(Logica.nextLookAndSeeNumber(firstValue) == nextValue);
	}
	
	@Test
	public void testNextLookAndSeeNumber4() {
		
		long firstValue = 1211;
		long nextValue = 111221;
		
		assertTrue(Logica.nextLookAndSeeNumber(firstValue) == nextValue);
	}
	
	@Test
	public void testNextLookAndSeeNumber5() {
		
		long firstValue = 111221;
		long nextValue = 312211;
		
		assertTrue(Logica.nextLookAndSeeNumber(firstValue) == nextValue);
	}
	
	@Test
	public void testIsDateValid1() {
		
		int day = 28;
		int month = 2;
		int year = 2017;
		
		assertTrue(Logica.isDateValid(day, month, year));
	}
	
	@Test
	public void testIsDateValid2() {
		
		int day = 29;
		int month = 2;
		int year = 2017;
		
		assertFalse(Logica.isDateValid(day, month, year));
	}
	
	@Test
	public void testIsDateValid3() {
		
		int day = 30;
		int month = 11;
		int year = 2017;
		
		assertTrue(Logica.isDateValid(day, month, year));
	}
	
	@Test
	public void testIsDateValid4() {
		
		int day = 31;
		int month = 11;
		int year = 2017;
		
		assertFalse(Logica.isDateValid(day, month, year));
	}
	
	@Test
	public void testToRomanNumeral1() {
		
		int number = 4;
		String roman = "IV";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral2() {
		
		int number = 9;
		String roman = "IX";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral3() {
		
		int number = 40;
		String roman = "XL";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral4() {
		
		int number = 90;
		String roman = "XC";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral5() {
		
		int number = 400;
		String roman = "CD";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral6() {
		
		int number = 900;
		String roman = "CM";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral7() {
		
		int number = 1904;
		String roman = "MCMIV";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral8() {
		
		int number = 1954;
		String roman = "MCMLIV";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral9() {
		
		int number = 1990;
		String roman = "MCMXC";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}
	
	@Test
	public void testToRomanNumeral10() {
		
		int number = 2014;
		String roman = "MMXIV";
		
		assertTrue(Logica.toRomanNumeral(number).equals(roman));
	}

}
